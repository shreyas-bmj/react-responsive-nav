import styled from "styled-components";

export const CustomBody = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 89vh;
`;
