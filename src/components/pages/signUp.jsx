import React from 'react';
import { CustomBody } from "./customElements/bodyElements";

const SignUp = () => {
  return (
    <CustomBody>
      <h1>Sign Up</h1>
    </CustomBody>
  );
};

export default SignUp;